var mongoose = require('mongoose');
var CheckIn = mongoose.model('CheckIn');
var User = mongoose.model('User');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.checkin_list = function(req, res) {
    CheckIn.find(req.query, function(err, stadiums) {
        res.status(200);
        res.json({"message": "Got all data", "data": stadiums});
    });
};

module.exports.checkin_detail = function(req, res) {
    CheckIn.findById(req.params.id, function (err, status) {
        res.status(200);
        res.json({"message": "Got Status", "data": status});
    });
};

module.exports.checkin_create = function(req, res) {

    var checkin = new CheckIn();

    checkin.userId = req.body.userId;
    checkin.stadiumId = req.body.stadiumId;
    checkin.status = req.body.status;

    checkin.save(function(err) {
        res.status(200);
        res.json({"message": "Checked successfully", "data": checkin});
    });
};

module.exports.checkin_delete = function(req, res) {
    CheckIn.findByIdAndRemove(req.params.id, function (err, status) {
        res.status(200);
        res.json({"message": 'Status deleted successfully!', "data": status});
    });
};

module.exports.checkin_update = function(req, res) {
    CheckIn.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, status) {
        res.status(200);
        res.json({"message": 'Status udpated successfully!', "data": status});
    });
};

module.exports.percent = function(req, res) {
    var stadiumId = req.query.stadiumId;
    if (!stadiumId || stadiumId == '') {
        res.status(400);
        res.json({"message": "No Stadium ID", "data": 0});
    } else {
        User.count({}, function(err, c1) {
            CheckIn.count({'stadiumId': stadiumId, 'status': true }, function(err, c2) {
                res.status(200);
                var percent = (c2*100) / c1;
                res.json({"message": "Get Percent", "data": percent});
            });
        });
    }
};