var mongoose = require('mongoose');
var Stadium = mongoose.model('Stadium');
const uuidv5 = require('uuid/v5');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.stadium_list = function(req, res) {
    Stadium.find({}, function(err, stadiums) {
        res.status(200);
        res.json({"message": "Got all Stadiums", "data": stadiums});
    });
};

module.exports.stadium_detail = function(req, res) {
    Stadium.findById(req.params.id, function (err, stadium) {
        res.status(200);
        res.json({"message": "Got a Stadium.", "data": stadium});
    });
};

module.exports.stadium_create = function(req, res) {

    var stadium = new Stadium();

    stadium.name = req.body.name;
    stadium.image = req.body.image;
    stadium.lat = req.body.lat;
    stadium.long = req.body.long;

    stadium.save(function(err) {
        res.status(200);
        res.json({"message": "Stadium Created successfully", "data": stadium});
    });
};

module.exports.stadium_delete = function(req, res) {
    Stadium.findByIdAndRemove(req.params.id, function (err, stadium) {
        res.status(200);
        res.json({"message": 'Stadium Deleted successfully!', "data": stadium});
    });
};

module.exports.stadium_update = function(req, res) {
    Stadium.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, stadium) {
        res.status(200);
        res.json({"message": 'Stadium udpated successfully', "data": stadium});
    });
};

module.exports.stadium_image = function(req, res) {
    const file = req.files[0];
    res.status(200);
    res.json({"message": 'File uploaded', "data": file.path});
}