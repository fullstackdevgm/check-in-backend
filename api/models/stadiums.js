var mongoose = require( 'mongoose' );

var stadiumSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    lat: {
        type: Number,
        required: true
    },
    long: {
        type: Number,
        required: true
    }
});

mongoose.model('Stadium', stadiumSchema);
