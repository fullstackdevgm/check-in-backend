var mongoose = require( 'mongoose' );

var checkinSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    stadiumId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    status: {
        type: Boolean,
        required: true
    }
});

mongoose.model('CheckIn', checkinSchema);