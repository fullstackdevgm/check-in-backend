var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});

var ctrlProfile = require('../controllers/profile');
var ctrlAuth = require('../controllers/authentication');
var ctrlStadium = require('../controllers/stadium');
var ctrlCheckIn = require('../controllers/checkin');

router.get('/profile', auth, ctrlProfile.profileRead);

// authentication
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);

//stadiums
router.get('/stadiums', ctrlStadium.stadium_list);
router.post('/stadiums', ctrlStadium.stadium_create);
router.get('/stadiums/:id', ctrlStadium.stadium_detail);
router.put('/stadiums/:id', ctrlStadium.stadium_update);
router.delete('/stadiums/:id', ctrlStadium.stadium_delete);

//check-in status
router.get('/status', ctrlCheckIn.checkin_list);
router.post('/status', ctrlCheckIn.checkin_create);
router.get('/status/:id', ctrlCheckIn.checkin_detail);
router.put('/status/:id', ctrlCheckIn.checkin_update);
router.delete('/status/:id', ctrlCheckIn.checkin_delete);
router.get('/percent', ctrlCheckIn.percent);

module.exports = router;
