var express = require('express');
var router = express.Router();
var multer = require('multer');
const uuidv4 = require('uuid/v4');
var path = require('path');

var upload = multer({ dest: 'api/uploads/images/stadiums/' });

var ctrlStadium = require('../controllers/stadium');

router.post('/stadium-img', upload.any(), ctrlStadium.stadium_image);

module.exports = router;